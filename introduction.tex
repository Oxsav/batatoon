A \textit{web} começou desde cedo, na data de 1945, a ser pensada apresentando um sistema chamado \textit{Memex}, permitindo seguir \textit{links} e documentos em microfilme. Em 1960 é apresentado um sistema semelhante onde as ligações são feitas em documentos de texto, contudo, no mesmo ano já se falava em \textit{hypertext} \cite{w3history}.

Depois disso surgiu a criação da \textit{Advanced Research Projects Agency Network  (ARPANET)} e começaram a surgir protocolos de comunicação, como também, um sistema de email. Em 1989 Tim Berners-Lee apresenta a proposta \textit{Information Management: A Proposal}, que dizia respeito à gestão de informação sobre o acelerador de partículas e experimentações no CERN. Essa proposta aborda problemas de perda de informação em sistemas complexos e apresenta uma solução baseada no sistema de hypertext distribuído \cite{w3proposal}. No ano seguinte Tim começa a trabalhar na implementação de um \textit{browser} e num editor que permitisse o uso de \textit{hyperlinks} \cite{w3history}.

Todo este processo foi evoluindo ao longo dos anos e, nos dias de hoje, a \textit{Word Wide Web} (W3) é imprescindível no dia a dia das pessoas, seja num computador, num \textit{tablet} ou até mesmo num \textit{smartphone}. Face a esta grande evolução, várias discussões têm sido geradas acerca de aplicações nativas \textit{"versus"} aplicações \textit{web}. Por conseguinte, as grandes empresas estão cada vez mais a apostar em tecnologias \textit{web}, exemplo disso é a \textit{Google}, que tem uma série de serviços disponibilizados a partir de um \textit{web browser} (\textit{Gmail}, \textit{Drive Google}, \textit{PlayStore}, etc.). Para além desses serviços e com a ajuda da \textit{World Wide Web Consortium (W3C)} e da \textit{The Internet Engineering Task Force (IETF)}, foi possível criar uma tecnologia que permitisse a comunicação áudio e vídeo entre \textit{web browsers}, que resultou na tecnologia \textit{Web Real Time Communication (WebRTC)}. Com esta tecnologia é possível a criação de aplicações que permitam realizar comunicações em tempo real entre várias pessoas, como é o caso de algumas aplicações nativas (e.g., \textit{Skype}). Esta recente tecnologia foi apresentada em 2012 pela Google, atingindo uma rápida evolução, permitindo o surgimento de várias soluções utilizando as suas \textit{Application Programming Interfaces (APIs)}. 
Num futuro próximo, prevê-se  uma evolução exponencial do uso desta tecnologia não só ao nível de computadores, mas também em \textit{tablets} e \textit{smartphones}, como ilustrado na Figura \ref{fig:webrtcevolution}.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.7\textwidth]{images/webrtcevolution.jpg}
  \caption{Estimativa da evolução da tecnologia \textit{WebRTC} \cite{disruptiveanalysis}.}
  \label{fig:webrtcevolution}
\end{figure}


\section{Motivação e objetivos \label{sec:MotObj}}

A tecnologia \textit{WebRTC} tem vindo a evoluir desde que a \textit{Google}, o \textit{W3C} e o \textit{IETF} começaram a trabalhar na sua normalização \cite{article1}. Desde então, têm vindo a ser desenvolvidas várias aplicações \textit{WebRTC}, que serão apresentadas no decorrer desta dissertação. Em contraste das normas \textit{Voice over Internet Protocol (VoIP)}, que utilizam vulgarmente protocolos de sinalização como \textit{Session Initiation Protocol (SIP)}, \textit{WebRTC} deixa em aberto de que forma a sinalização entre \textit{peers} é efetuada, ficando esta decisão a cargo de quem desenvolve a aplicação.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.7\textwidth]{images/motivation.png}
  \caption{Exemplo de motivação.}
  \label{fig:motivation}
\end{figure}

A Figura \ref{fig:motivation}, esquematiza a principal motivação deste projeto, que deriva do facto de na camada aplicacional não existir um mecanismo normalizado para a troca de informação de sinalização entre \textit{peers}. Na tecnologia \textit{WebRTC} é necessário haver uma negociação de sinalização entre os diferentes \textit{peers}, de forma a criar ligações \textit{peer} entre eles, para que a informação \textit{media} e dados possa ser trocada. 

Neste projeto efetuou-se uma análise aprofundada à tecnologia \textit{WebRTC} e também a tecnologias que ajudem na troca de informação de sinalização entre os diferentes \textit{peers}. Atendendo a que a solução a desenvolver usará tecnologias web, por estratégia da empresa, o destaque a este nível incidirá nas tecnologias WebSockets.

Neste contexto, este projeto tem como objectivo o estudo de várias tecnologias \textit{WebSockets} e fazer uma análise comparativa entre essas diferentes tecnologias. Desta análise comparativa serão tomadas algumas decisões sobre as tecnologias que devem ser usadas no desenvolvimento de uma \textit{Framework} que tem por objectivo principal facilitar o desenvolvimento de serviços \textit{WebRTC} (tais como presença, histórico de chamadas, conferência, etc) a um alto nível. 

Para isso foram definidas as seguintes fases no desenvolvimento deste projeto:

\begin{itemize}

\item estudo da tecnologia \textit{WebRTC} e de soluções que permitam a comunicação entre \textit{web browsers};
\item definição de casos de uso e definição de uma arquitectura \textit{WebRTC} para \textit{Framework};
\item desenvolvimento da \textit{Framework} e definição da prova de conceito a realizar;
\item testes e análise de resultados.

\end{itemize}

A prova de conceito servirá, essencialmente, para verificar se os serviços desenvolvidos na \textit{Framework} se encontram operacionais. Para esse efeito, usando esta \textit{Framework}, foi desenvolvido um cliente \textit{WebRTC}, implementando os serviços de presença, histórico de chamadas, chamadas áudio e vídeo, partilha de ficheiros, partilha de ecrã e \textit{chat}. 

Para além dos testes de operacionalidade dos serviços, foram realizados testes ao nível de utilização de memória e \textit{Central Processing Unit (CPU)}, tanto nos clientes como nos servidores. Desta forma poder-se-á verificar qual o peso computacional da tecnologia \textit{WebRTC} nas diferentes máquinas. Tal permitirá ainda verificar se o servidor é usado durante uma chamada / conferência entre diferentes utilizadores, ou se é apenas necessário para a negociação da sinalização entre \textit{peers}. Desta forma, é possível verificar se as ligações são realmente \textit{peer-to-peer}, ou se o acesso ao servidor é recorrente para os \textit{peers} realizarem essa comunicação.

\section{Principais contribuições}

Uma das discussões que se encontra mais em voga é relativa ao uso \textit{web "versus"} aplicações nativas. Maior parte das análises realizadas fazem uma avaliação em termos de desempenho, verificando até que ponto seria viável a criação de aplicações \textit{web} em vez de aplicações nativas. Neste sentido, existem cada vez mais aplicações \textit{web} que fazem o mesmo que as várias aplicações nativas, cuja a única diferença existente é no facto das aplicações \textit{web} correrem sobre um \textit{web browser}. 

A tecnologia \textit{WebRTC} permite realizar comunicações entre diferentes clientes em tempo real. Como este projeto está a ser desenvolvido num âmbito empresarial, pretende-se tirar partido da \textit{WebRTC} ser uma tecnologia recente, estudando-a em profundidade. Desta forma, pode-se verificar que vantagens poderá trazer no futuro das telecomunicações e que serviços poderão ser implementados, em comparação às aplicações nativas já existentes.

Assim, as contribuições deste trabalho surgem pelo estudo e classificação dos conceitos associados à \textit{WebRTC} e, numa prespectiva mais prática, pelo desenvolvimento de uma \textit{Framework} que permita a outras entidades desenvolver soluções \textit{WebRTC}, a um mais alto nível e de uma forma mais simplificada. Para além disso, foi desenvolvido um cliente \textit{WebRTC}, para verificar as funcionalidades da \textit{Framework} desenvolvida. Assim sendo é possível explorar e estudar mais aprofundadamente a tecnologia \textit{WebRTC}, analisando as vantagens e desvantagens desta tecnologia, como também estar ao corrente de todas as atualizações e modificações feitas na tecnologia.

Como fruto deste trabalho, foi elaborado e submetido um artigo científico na "13ª Conferência de Redes de Computadores"  \cite{crc13}, já aceite para publicação. Foi também realizado um artigo, sendo este publicado na Revista Saber e Fazer Telecomunicações, da empresa em questão.

\section{Organização da dissertação}

No presente capítulo, Capítulo \ref{cha:Introduction}, é feita uma introdução aos conceitos das tecnologias que são abordadas nesta dissertação. É referida a principal motivação e os objectivos deste projeto, como também quais as principais contribuições.

No Capítulo \ref{cha:Chap2} abordam-se os conceitos associados à tecnologia WebRTC, apresentando-se as \textit{APIs} que a constituiem, que protocolos são usados, os \textit{codecs} e a compatibilidade relativamente aos \textit{web browsers} mais usados atualmente. É também abordada a componente de sinalização, onde é referido o protocolo \textit{WebSockets}.

No Capítulo \ref{cha:Chap3}, relativo ao estado da arte, são apresentadas várias soluções que usam as diferentes \textit{APIs} da tecnologia \textit{WebRTC}. Para além de aplicações existentes, são apresentadas bibliotecas, repositórios e \textit{frameworks}, que se encontram desenvolvidas e ainda são descritas soluções relativamente a \textit{WebSockets}. 

No Capítulo \ref{cha:Chap4} são delineados os casos de uso e requisitos definidos para a aplicação cliente e \textit{framework} a desenvolver neste projeto. Apresenta-se a arquitetura e estrutura de todo o sistema desenvolvido, bem como as mensagens trocadas entre os módulos do servidor e também com os clientes. 

No Capítulo \ref{cha:Chap5} são apresentados resultados dos testes realizados no decorrer do projeto. São apresentados dois cenários com características diferentes, para se poder analisar comportamentos relativamente aos sistemas e à rede. 

No Capítulo \ref{cha:Conclusions} são apresentadas as principais conclusões resultantes do trabalho e pesquisa efetuada, quais as principais contribuições deste projeto e é também feita uma análise relativamente ao trabalho futuro.
