\select@language {portuguese}
\contentsline {chapter}{Agradecimentos}{iii}
\contentsline {chapter}{Abstract}{v}
\contentsline {chapter}{Resumo}{vii}
\contentsline {chapter}{Conte\IeC {\'u}do}{ix}
\contentsline {chapter}{Lista de Figuras}{xv}
\contentsline {chapter}{Lista de Tabelas}{xix}
\contentsline {chapter}{Lista de Acr\IeC {\'o}nimos}{xxi}
\contentsline {chapter}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o }{1}
\contentsline {section}{\numberline {1.1}Motiva\IeC {\c c}\IeC {\~a}o e objetivos }{2}
\contentsline {section}{\numberline {1.2}Principais contribui\IeC {\c c}\IeC {\~o}es}{4}
\contentsline {section}{\numberline {1.3}Organiza\IeC {\c c}\IeC {\~a}o da disserta\IeC {\c c}\IeC {\~a}o}{5}
\contentsline {chapter}{\numberline {2}Conceitos Introdut\IeC {\'o}rios }{7}
\contentsline {section}{\numberline {2.1}\textit {WebRTC}}{7}
\contentsline {subsection}{\numberline {2.1.1}WebRTC C++ API}{8}
\contentsline {subsection}{\numberline {2.1.2}\textit {WebRTC API}}{9}
\contentsline {subsubsection}{\textit {PeerConnection}}{9}
\contentsline {subsubsection}{\textit {Media Streams}}{9}
\contentsline {subsubsection}{\textit {RTC Data Channels}}{10}
\contentsline {section}{\numberline {2.2}Compatibilidade}{10}
\contentsline {section}{\numberline {2.3}Protocolos \textit {data channel}}{11}
\contentsline {subsection}{\numberline {2.3.1}\textit {Stream Control Transmission Protocol (SCTP)}}{12}
\contentsline {subsection}{\numberline {2.3.2}\textit {Datagram Transport Layer Security (DTLS)}}{12}
\contentsline {subsection}{\numberline {2.3.3}\textit {User Datagram Protocol (UDP)}}{12}
\contentsline {subsection}{\numberline {2.3.4}\textit {Secure Real-Time Protocol - Datagram Transport Layer Security}}{13}
\contentsline {subsection}{\numberline {2.3.5}\textit {Session Description Protocol (SDP)}}{13}
\contentsline {section}{\numberline {2.4}\textit {WebSockets}}{14}
\contentsline {section}{\numberline {2.5}Sinaliza\IeC {\c c}\IeC {\~a}o}{15}
\contentsline {subsection}{\numberline {2.5.1}\textit {SIP over WebSockets}}{15}
\contentsline {subsection}{\numberline {2.5.2}\textit {Jingle over WebSockets}}{16}
\contentsline {subsection}{\numberline {2.5.3}\textit {JavaScript Session Establishment Protocol (JSEP)}}{17}
\contentsline {section}{\numberline {2.6}\textit {Network Address Translation (NAT)}}{18}
\contentsline {subsection}{\numberline {2.6.1}\textit {Interactive Connectivity Establishment (ICE)}}{18}
\contentsline {subsection}{\numberline {2.6.2}\textit {Session Traversal Utilities for NAT (STUN)}}{18}
\contentsline {subsection}{\numberline {2.6.3}\textit {Traversal Using Relays around NAT (TURN)}}{19}
\contentsline {section}{\numberline {2.7}\textit {Codecs}}{19}
\contentsline {section}{\numberline {2.8}Sum\IeC {\'a}rio}{20}
\contentsline {chapter}{\numberline {3}Solu\IeC {\c c}\IeC {\~o}es \textit {WebRTC} }{21}
\contentsline {section}{\numberline {3.1}Solu\IeC {\c c}\IeC {\~o}es existentes}{21}
\contentsline {subsection}{\numberline {3.1.1}\textit {PeerCDN}}{21}
\contentsline {subsection}{\numberline {3.1.2}\textit {Conversat.io}}{22}
\contentsline {subsection}{\numberline {3.1.3}\textit {BananaBread}}{22}
\contentsline {subsection}{\numberline {3.1.4}\textit {Responsive Web Typography with WebRTC}}{22}
\contentsline {subsection}{\numberline {3.1.5}\textit {SIPML5}}{23}
\contentsline {section}{\numberline {3.2}Bibliotecas \textit {JavaScript}}{23}
\contentsline {subsection}{\numberline {3.2.1}\textit {WebRTC Experiment}}{24}
\contentsline {subsubsection}{\textit {RCMultiConnection.js}}{24}
\contentsline {subsubsection}{\textit {DataChannel.js}}{25}
\contentsline {subsubsection}{\textit {RecordRTC.js}}{25}
\contentsline {subsubsection}{\textit {RTCCall.js}}{25}
\contentsline {subsection}{\numberline {3.2.2}\textit {SimpleWebRTC}}{26}
\contentsline {subsection}{\numberline {3.2.3}\textit {Adapter.js}}{26}
\contentsline {section}{\numberline {3.3}WebSockets}{27}
\contentsline {subsection}{\numberline {3.3.1}\textit {Node.js}}{27}
\contentsline {subsection}{\numberline {3.3.2}\textit {Vert.x}}{28}
\contentsline {subsection}{\numberline {3.3.3}\textit {Google App Engine}}{30}
\contentsline {section}{\numberline {3.4}\textit {Gateways} para \textit {SIP}}{30}
\contentsline {subsection}{\numberline {3.4.1}\textit {Asterisk}}{31}
\contentsline {subsection}{\numberline {3.4.2}\textit {WebRTC2SIP (sipML5)}}{31}
\contentsline {section}{\numberline {3.5}Experimenta\IeC {\c c}\IeC {\~a}o e sele\IeC {\c c}\IeC {\~a}o de solu\IeC {\c c}\IeC {\~o}es existentes}{33}
\contentsline {subsection}{\numberline {3.5.1}\textit {Vert.x vs Node.js}}{33}
\contentsline {subsubsection}{\textit {Vert.x vs node.js simple HTTP benchmarks}}{34}
\contentsline {subsubsection}{Compara\IeC {\c c}\IeC {\~a}o de \textit {server side websockets} utilizando \textit {atmosphere}, \textit {netty}, \textit {node.js} e \textit {vert.x}}{35}
\contentsline {subsection}{\numberline {3.5.2}Bibliotecas \textit {JavaScript WebRTC}}{37}
\contentsline {subsubsection}{\textit {WebRTC Experiment vs SimpleWebRTC}}{37}
\contentsline {subsubsection}{Base de dados}{38}
\contentsline {subsubsection}{Linguagens de Programa\IeC {\c c}\IeC {\~a}o}{38}
\contentsline {section}{\numberline {3.6}Sum\IeC {\'a}rio}{39}
\contentsline {chapter}{\numberline {4} \textit {Framework Webrtc} }{41}
\contentsline {section}{\numberline {4.1}Requisitos funcionais}{41}
\contentsline {subsection}{\numberline {4.1.1}Chamada com controlo de estabelecimento de chamada}{41}
\contentsline {subsection}{\numberline {4.1.2}Chamada b\IeC {\'a}sica com controlo completo da chamada}{42}
\contentsline {subsection}{\numberline {4.1.3}Chamada com pessoas externas}{42}
\contentsline {subsection}{\numberline {4.1.4}Presen\IeC {\c c}a e Lista de contactos}{42}
\contentsline {subsection}{\numberline {4.1.5}Chat / Mensagens Instant\IeC {\^a}neas}{43}
\contentsline {subsection}{\numberline {4.1.6}Partilha de Ficheiros}{43}
\contentsline {subsection}{\numberline {4.1.7}Grava\IeC {\c c}\IeC {\~a}o de Voz e V\IeC {\'\i }deo}{44}
\contentsline {section}{\numberline {4.2}Arquitetura da \textit {Framework}}{44}
\contentsline {subsection}{\numberline {4.2.1}Especifica\IeC {\c c}\IeC {\~o}es Iniciais}{44}
\contentsline {subsection}{\numberline {4.2.2}Estrutura arquitetural}{46}
\contentsline {section}{\numberline {4.3}Descri\IeC {\c c}\IeC {\~a}o dos M\IeC {\'o}dulos}{47}
\contentsline {subsection}{\numberline {4.3.1}M\IeC {\'o}dulo servidor \textit {web}}{47}
\contentsline {subsection}{\numberline {4.3.2}M\IeC {\'o}dulo de base de dados}{48}
\contentsline {subsection}{\numberline {4.3.3}M\IeC {\'o}dulo de gest\IeC {\~a}o de autentica\IeC {\c c}\IeC {\~o}es e autoriza\IeC {\c c}\IeC {\~o}es}{49}
\contentsline {subsection}{\numberline {4.3.4}M\IeC {\'o}dulo de gest\IeC {\~a}o de sess\IeC {\~o}es}{50}
\contentsline {subsection}{\numberline {4.3.5}M\IeC {\'o}dulo de registo utilizador}{51}
\contentsline {subsection}{\numberline {4.3.6}M\IeC {\'o}dulo de pesquisa e intersec\IeC {\c c}\IeC {\~a}o de servi\IeC {\c c}os}{52}
\contentsline {subsection}{\numberline {4.3.7}M\IeC {\'o}dulo de gest\IeC {\~a}o de utilizadores}{52}
\contentsline {subsection}{\numberline {4.3.8}M\IeC {\'o}dulo de presen\IeC {\c c}a}{53}
\contentsline {subsection}{\numberline {4.3.9}M\IeC {\'o}dulo de hist\IeC {\'o}rico de chamadas}{53}
\contentsline {subsection}{\numberline {4.3.10}Sinaliza\IeC {\c c}\IeC {\~a}o}{54}
\contentsline {section}{\numberline {4.4}Mensagens}{54}
\contentsline {subsection}{\numberline {4.4.1}Registo de cliente}{54}
\contentsline {subsection}{\numberline {4.4.2}Autentica\IeC {\c c}\IeC {\~a}o de Cliente}{57}
\contentsline {subsection}{\numberline {4.4.3}M\IeC {\'o}dulo de presen\IeC {\c c}a e Intera\IeC {\c c}\IeC {\~a}o no sistema}{60}
\contentsline {subsection}{\numberline {4.4.4}M\IeC {\'o}dulo de gest\IeC {\~a}o de utilizadores}{63}
\contentsline {section}{\numberline {4.5}Confer\IeC {\^e}ncias \IeC {\'a}udio e v\IeC {\'\i }deo}{64}
\contentsline {section}{\numberline {4.6}Sum\IeC {\'a}rio}{68}
\contentsline {chapter}{\numberline {5}Cen\IeC {\'a}rios de teste e resultados obtidos }{71}
\contentsline {section}{\numberline {5.1}Testes de valida\IeC {\c c}\IeC {\~a}o}{71}
\contentsline {section}{\numberline {5.2}Testes de desempenho computacional}{72}
\contentsline {subsection}{\numberline {5.2.1}Cen\IeC {\'a}rio 1 - Rede empresarial}{74}
\contentsline {subsection}{\numberline {5.2.2}Cen\IeC {\'a}rio 2 - Rede dom\IeC {\'e}stica}{78}
\contentsline {section}{\numberline {5.3}Sum\IeC {\'a}rio}{80}
\contentsline {chapter}{\numberline {6}Conclus\IeC {\~o}es e trabalho futuro}{83}
\contentsline {section}{\numberline {6.1}Resumo do trabalho desenvolvido}{83}
\contentsline {section}{\numberline {6.2}Principais contribui\IeC {\c c}\IeC {\~o}es}{85}
\contentsline {section}{\numberline {6.3}Trabalho futuro}{85}
\contentsline {chapter}{\numberline {A}Anexo A - Mensagem SDP}{87}
\contentsline {chapter}{\numberline {B}Anexo B}{91}
\contentsline {chapter}{\numberline {C}Anexo C}{93}
\contentsline {section}{\numberline {C.1}Cen\IeC {\'a}rio 1}{93}
\contentsline {section}{\numberline {C.2}Cen\IeC {\'a}rio 2}{94}
\contentsline {chapter}{Bibliografia}{97}
