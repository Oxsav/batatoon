O presente capítulo aborda os conceitos introdutórios deste projeto. São apresentados os conceitos de \textit{WebRTC} e as suas \textit{APIs}, a compatibilidade atual. De seguida são descritos os protocolos relacionados com a \textit{API DataChannel WebRTC}, o protocolo WebSockets e ainda os métodos de sinalização que são normalmente usados neste tipo de soluções. Apresenta-se ainda o problema existente de \textit{Network Address Translation (NAT)} relacionado com a tecnologia \textit{WebRTC}. Por fim abordam-se os \textit{codecs} utilizados por cada browser.

\section{\textit{WebRTC}}

As \textit{Real Time Communications (RTC)}, através da \textit{web} têm vindo a evoluir devido a dois organismos de normalização – \textit{Internet Engineering Task Force (IETF)} e \textit{World Wide Web Consortium (W3C)}.
\textit{WebRTC} é um projeto \textit{open source}, grátis e normalizado que permite aos browsers realizarem comunicações em tempo real. Um dos principais objetivos é desenvolver aplicações em \textit{web browsers}, com a ajuda de \textit{APIs} de \textit{JavaScript} e \textit{HyperText Markup Language 5} (\textit{HTML5}) \cite{WebRTCSite}.

Esta tecnologia permite ainda  o acesso seguro a componentes internos (como a \textit{webcam} e/ou microfone) de qualquer sistema, desta forma é possível receber e enviar informação (\textit{media}) \textit{peer-to-peer} (\textit{P2P}), em tempo real entre dois \textit{browsers}, sem qualquer tipo de \textit{plugin}.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.9\textwidth]{images/webrtcarch.png}
  \caption{Arquitetura \textit{WebRTC} \cite{WebRTCSite}.}
  \label{fig:webrtc_arch_apis}
\end{figure}

A Figura \ref{fig:webrtc_arch_apis}, ilustra a arquitetura da tecnologia WebRTC, onde se verifica que é constituída por duas \textit{APIs} diferentes: \textit{WebRTC C++ API} e \textit{Web API}. A \textit{WebRTC C++ API} é utilizada por programadores, que queiram desenvolver \textit{web browsers}, enquanto a \textit{Web API} é usada por pessoas que queiram desenvolver aplicações \textit{web}, com o objetivo de tirar proveito das funcionalidades desta tecnologia.

Como este projeto consiste na criação de uma aplicação \textit{WebRTC}, na secção seguinte é apresentada a \textit{Web API WebRTC} e os conceitos associados, a compatibilidade atual desta tecnologia nos \textit{web browsers} e quais os protocolos usados para comunicação e para sinalização.

\subsection{WebRTC C++ API}

A WebRTC C++ é a API que é utilizada para o desenvolvimento de \textit{web browsers}, no que diz respeito à implementação \textit{WebRTC}. Sendo esta tecnologia \textit{open source}, o código fonte encontra-se disponível para qualquer pessoa \cite{webrtcCode}. 
Para além do desenvolvimento de \textit{web browsers}, é possível estar atento a novas atualizações e até mesmo ajudar a encontrar erros, visto que todo o código se encontra num repositório \textit{Subversion (SVN)}. 


\subsection{\textit{WebRTC API}}

Esta \textit{API} permite desenvolver aplicações em \textit{web browsers}, o que fez com que a linguagem de programação adotada fosse \textit{JavaScript}, assim as aplicações são desenvolvidas do lado do cliente, o que permite explorar as capacidades em tempo real dos browsers. A API WebRTC é definida sobre três principais conceitos \textit{PeerConnection}, \textit{MediaStreams} e \textit{DataChannel}, apresentados a seguir \cite{html5Webrtc, johnston2012webrtc}.

\subsubsection{\textit{PeerConnection}}

\textit{PeerConnection} permite que dois utilizadores comuniquem diretamente, \textit{browser-to-browser}. Para estabelecer esta ligação e haver uma negociação de sinalização, é necessário que haja um canal de sinalização. Este é implementado num servidor \textit{Web}, utilizando \textit{WebSockets} ou \textit{XMLHttpRequest}. Este mecanismo usa o protocolo \textit{Interactive Connectivity Establishment (ICE)} juntamente com \textit{Session Traversal Utilities for NAT (STUN)} e \textit{Traversal Using Relays arround NAT (TURN)} para permitir ajudar as \textit{streams} de \textit{media} a passarem por \textit{Network Address Translation (NAT)} e \textit{firewalls} \cite{html5Webrtc, johnston2012webrtc, w3cWebRTC}.

\subsubsection{\textit{Media Streams}}

\textit{MediaStream} é uma forma abstrata de representar uma \textit{stream} de dados áudio e/ou vídeo. Este tipo de aplicações pode ser usada para mostrar, gravar ou enviar o seu conteúdo para um \textit{peer} remoto. Existem dois tipos de \textit{stream}: \textit{Local MediaStream} ou \textit{Remote MediaStream}. \textit{Local MediaStream} é a \textit{stream} capturada no próprio sistema (\textit{webcam} e microfone) enquanto que \textit{Remote MediaStream} é a \textit{stream} recebida de outro \textit{peer}. Para ter acesso à \textit{media} dos componentes do terminal é necessário executar a função \textit{getUserMedia()}, onde podem ser definidos alguns parâmetros, dependendo do que o utilizador do terminal queira reproduzir.  Para a transmissão das \textit{streams} são usados protocolos como \textit{Secure Real-time Transport Protocol (SRTP)}, \textit{Real-time Transport Protocol (RTP)} e \textit{Real-time Transport Control Protocol (RTCP)} para monitorização de transmissão de \textit{media}. O Protocolo \textit{Datagram Transport Layer Security (DTLS)} é usado como uma chave de \textit{SRTP} e para gestão de associações \cite{html5Webrtc, johnston2012webrtc, w3cWebRTC}.


\subsubsection{\textit{RTC Data Channels}}

\textit{RTCDataChannel} é um canal de dados bidirecional em ligações \textit{peer-to-peer}. É possível serem transferidos dados que não sejam \textit{media} e é necessário usar outro protocolo, como \textit{SCTP} encapsulado em \textit{DTLS}. Desta forma existe uma solução para \textit{NAT} com confidencialidade, autenticação da fonte e integridade dos dados que sejam necessários transferir. O \textit{SCTP} permite a entrega, fiável e não fiável, de dados e permite que as aplicações abram várias \textit{streams} independentes. Para a criação de um \textit{DataChannel} é necessário executar a função \textit{CreateDataChannel()} numa instância da \textit{PeerConnection} \cite{html5Webrtc, johnston2012webrtc, w3cWebRTC}.

\section{Compatibilidade}

Por \textit{WebRTC} ser uma tecnologia \textit{web}, esta não depende apenas de si própria, assim, é essencial garantir que haja compatibilidade nos diferentes \textit{web browsers}. 

É então necessário garantir que os \textit{web browsers} suportem \textit{HTML5}, que é um novo \textit{standard} para a \textit{syntax HTML}. Suporta novas funcionalidades, 
como a redução da utilização de \textit{plugins} adicionais como \textit{flash}, novas \textit{tags} que permitem substituir \textit{scripting} e melhoramentos no tratamento de erros.

Juntando \textit{HTML5} e \textit{WebRTC}, é possível ter uma solução \textit{web} para realização de chamadas áudio e vídeo, pois \textit{WebRTC} tira proveito das \textit{tags} áudio e vídeo para reprodução do conteúdo de \textit{streams}. Isto é feito sem qualquer tipo de \textit{plugin} adicional, desde que o \textit{web browser} suporte essas funcionalidades.

As três \textit{APIs} que o \textit{WebRTC} implementa, já são suportadas em alguns \textit{browsers}, mas com algum trabalho por realizar. A tabela \ref{table:browserswebrtcsupport} apresenta o suporte dos \textit{web browsers} em relação a essas \textit{APIs}.

\begin{table}[h!]
	\caption{Versões dos \textit{web browsers} de suporte às \textit{APIs WebRTC} \cite{html5Webrtc}.}
	\begin{center}
	    \begin{tabular}{ | l | l | l | l | l | p{5cm} |}
	    \hline
	    API & Internet Explorer & Firefox & Chrome & Opera \\ \hline
	    MediaStream & Não Suporta & Suporta \textgreater v17 & Suporta \textgreater v18 & Suporta \textgreater v12 \\ \hline
	    Peer Connection & Não Suporta & Suporta \textgreater v22 & Suporta \textgreater v20 & Sem Informação \\ \hline
	    RTC Data Channels & Não Suporta & Suporta \textgreater v22 & Suporta \textgreater v26 & Sem Informação \\ 
	    \hline
	    \end{tabular}
	\end{center}
	\label{table:browserswebrtcsupport}
\end{table}

A tabela \ref{table:percbrowsersusage} permite perceber quais os \textit{web browsers} mais utilizados entre março e julho de 2013.

\begin{table}[h!]
	\caption{Percentagem dos \textit{web browsers} usados \cite{w3schbrowserstat}.}
	\begin{center}
	    \begin{tabular}{ | l | l | l | l | l | l | p{5cm} |}
	    \hline
	    2013 & Internet Explorer & Firefox & Chrome & Opera & Safari \\ \hline
	    Julho & 11,8\% & 28,9\% & 52,8\% & 1,6\% & 3,6\% \\ \hline
	    Junho & 12,0\% & 28,9\% & 52,1\% & 1,7\% & 3,9\% \\ \hline
	    Maio & 12,6\% & 27,7\% & 52,9\% & 1,6\% & 4,0\% \\ \hline
	    Abril & 12,7\% & 27,9\% & 52,7\% & 1,7\% & 4,0\% \\ \hline
	    Março & 13,0\% & 28,5\% & 51,7\% & 1,8\% & 4,1\% \\ 
	    \hline
	    \end{tabular}
	\end{center}
	\label{table:percbrowsersusage}
\end{table}

 Este tipo de informação é importante pois é necessário saber quais os \textit{web browsers} mais usados recentemente, de forma a que possa haver uma interoperabilidade entre eles no que diz respeito a estas novas tecnologias.

\section{Protocolos \textit{data channel}}

Para além de \textit{media} na tecnologia \textit{WebRTC}, que é trocada sobre \textit{peer connections} em canais de \textit{media} (RTP), existe também a possibilidade de trocar dados entre os utilizadores de uma sessão. Foi assim que surgiram os canais de dados (\textit{Data Channels}) nesta tecnologia. Os \textit{Data Channels} usam quatro tipo de protocolos, \textit{Stream Control Transmission Protocol (SCTP)} para controlo de \textit{streams}, \textit{Datagram Transport Layer Security (DTLS)} para segurança dos dados e \textit{UDP/ICE} que são definidos como protocolos da camada de transporte.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.2\textwidth]{images/stack_datachannel.png}
  \caption{Pilha \textit{Data Channel} \cite{rtcdatachannel}.}
  \label{fig:stackdatachannel}
\end{figure}

A figura \ref{fig:stackdatachannel} representa a \textit{stack} base para os \textit{Data Channels}, em que \textit{SCTP} se encontra encapsulado em \textit{DTLS}, que por sua vez se encontram encapsulados em \textit{ICE/UDP} \cite{rtcdatachannel}.

\subsection{\textit{Stream Control Transmission Protocol  (SCTP)}}

O \textit{SCTP} é um protocolo fiável que trabalha na camada de transporte, de redes que não garantem entrega de pacotes no destino, exemplo disso é a rede \textit{Internet Protocol (IP)}. É um protocolo que oferece serviços como, entrega livre de erros, garantia que os dados não são entregues duplicados, fragmentação de dados de acordo com o \textit{Maximum Transmission Unit (MTU)}, entrega sequencial de mensagens de utilizador em múltiplas \textit{streams} com opção de entrega ordenada, tolerância a falhas ao nível da rede, devido ao suporte de \textit{multi-homing} nos terminais de associação \cite{SCTPRFC}. Para além destes serviços este protocolo implementa métodos para evitar congestão de dados e evita ataques de \textit{flooding} e \textit{masquerade} \cite{SCTPRFC}.

\subsection{\textit{Datagram Transport Layer Security (DTLS)}}

O \textit{DTLS} é um protocolo usado para permitir a segurança de tráfego na rede, assim, este, é como \textit{"TLS over Datagram"} onde a sua utilização tem como objetivo proteger dados em aplicações de comunicação. \textit{DTLS} é uma solução criada devido ao aparecimento de protocolos ao nível aplicacional, que usam como protocolo de transporte \textit{User Datagram Protocol (UDP)}.

\textit{DTLS} é uma variante do \textit{TLS}, para protocolos que usam \textit{UDP}. A sua definição permitiu usar a maior parte da infraestrutura e código que já tinha sido definido no \textit{TLS} \cite{DTLSRFC}. 
Foi então desenhado de forma a proteger dados em aplicações de comunicação, que não oferecem fiabilidade ou qualquer ordem de entrega de dados. Quando usado, não traz diferenças no que diz respeito a atrasos nas aplicações e para além disso não trata de perdas nem de ordenar os dados \cite{DTLSRFC}.


\subsection{\textit{User Datagram Protocol (UDP)}}

\textit{User Datagram Protocol (UDP)} permite transmitir mensagens com o mínimo \textit{overhead} protocolar e sendo um protocolo base da família TCP/IP, sem prévio estabelecimento de conexões. Este protocolo é normalmente usado para transportar pequenos pacotes de dados e para trocar media em sessões \textit{Real Time Protocol (RTP)}. 
Uma vez que as aplicações de comunicação em tempo real não são tolerantes a falhas ou a atrasos, logo é necessário garantir uma entrega rápida de pacotes de dados. O protocolo \textit{UDP} responde a essas exigências, porém não garante entrega ordenada dos dados, não retransmite pacotes falhados e não tem controlo de congestão. Existem métodos ao nível aplicacional que podem ajudar na resolução de alguns destes problemas, como \textit{codecs}, sistema de redução da largura de banda enquanto existe congestão \cite{UDPRFC}.

Este protocolo é fornecido pelo sistema operativo sob o \textit{web browser}.

\subsection{\textit{Secure Real-Time Protocol - Datagram Transport Layer Security}}

O \textit{RTP} é um protocolo de entrega de serviços fim-a-fim em tempo real, usado em redes \textit{IP}. Inicialmente foi desenvolvido para aplicações onde existem vários participantes, ou seja, conferências multimédia (áudio e vídeo). Atualmente é usado por diferentes aplicações \textit{peer-to-peer}, tais como, \textit{Simple Multicast Audio}, conferências áudio e vídeo, \textit{Mixers} and \textit{Translators} e \textit{Layered Encodings} \cite{RTPRFC}. No entanto, o
\textit{RTP} não garante \textit{qualidade de serviço (QoS)} ou entrega de pacotes de dados, contudo consegue uma reconstrução temporal, deteção de perda, segurança e identificação de conteúdo \cite{RTPRFC, DTLSSRTPRFC}.

O \textit{SRTP} tal como o protocolo \textit{RTP} fornece confidencialidade e autenticação de mensagens. A solução \textit{DTLS-SRTP} está definida para sessões \textit{media point-to-point}, onde se encontram apenas dois participantes. 

\textit{DTLS} é usado como uma extensão do \textit{SRTP}, uma vez que este não permite uma gestão de chaves nativa, assim, é necessário recorrer a mecanismos externos. Integra essa gestão de chaves, negociação de parâmetros e transferência segura de dados, desta forma é possível ter uma solução que combina desempenho e encriptação do SRTP, com a gestão de chaves e associações do DTLS \cite{DTLSSRTPRFC}.
Esta é a solução que WebRTC usa para a encriptação de dados RTP e gestão de chaves entre dois clientes.


\subsection{\textit{Session Description Protocol (SDP)}}

\textit{SDP} é um protocolo que permite a descrição de uma sessão entre dois clientes. Quando se inicia uma conferência multimédia, chamadas \textit{Voice over IP (VoIP)}, \textit{streaming} de vídeo, ou outro tipo de sessão, é necessário ter em conta algumas características, como os detalhes de \textit{media} (e.g. \textit{codecs}), os endereços de transporte, e outros dados da sessão que precisam de ser trocados entre os participantes \cite{SDP1}. É um protocolo completamente de texto com uma grande quantidade de informação, mas que é essencial para estabelecer uma sessão de \textit{media} entre os clientes. Este protocolo é usado numa grande quantidade de aplicações e diferentes tipos de redes, no entanto, não suporta nenhum tipo de negociação de sessões. A descrição de uma sessão SDP inclui o propósito e o nome da sessão, o tempo que a sessão está ativa e informação para compreender os meios de comunicação da sessão e a informação necessária para receber esses meios (endereços, portas, formatos, etc). Caso os recursos sejam limitados, convém incluir informação sobre a largura de banda usada na sessão e sobre o contacto da pessoa que está responsável pela sessão \cite{SDP1}.

No caso da tecnologia \textit{WebRTC}, a informação é codificada num objeto designado por \textit{RTCSessionDescription} \cite{html5Webrtc, w3cWebRTC}.
Este objeto serve para descrever a sessão entre dois clientes e as características \textit{media} de uma ligação \textit{peer} (\textit{Peer Connection}). Este processo na tecnologia \textit{WebRTC} é conhecida pelos métodos \textit{offer} e \textit{answer}. A \textit{offer} leva uma descrição sobre os protocolos, os \textit{media} e os \textit{codecs} que o cliente emissor suporta, já no caso de \textit{answer}, é uma resposta originada caso seja recebida a \textit{offer} e leva a mesma informação, mas neste caso do cliente recetor \cite{html5Webrtc, w3cWebRTC}.

\section{\textit{WebSockets}}\label{sec:websocket}

A criação de aplicações \textit{web} tem sido efetuada com base no mecanismo pedido/resposta do protocolo \textit{HTTP}, entre cliente e servidor. Depois da informação de uma página \textit{HTTP} ser carregada é necessário que exista uma interação do utilizador, como por exemplo, abrir outra página, para que haja uma atualização da informação que é mostrada.
Alguns destes problemas vieram ser resolvidos com o aparecimento do AJAX tornando os sítios mais dinâmicos, porém teria de haver sempre uma interação do utilizador ou um temporizador que atualizasse a página, para ver os dados atualizados. As tecnologias que permitem que o servidor envie dados atualizados mal os recebam, foram usadas desde algum tempo. Uma das soluções era a sondagem longa, que permite a abertura de uma ligação ao servidor, até que este receba um pedido. Um dos principais problemas deste tipo de soluções era o overhead de HTTP, que poderia prejudicar soluções que não são tolerantes a atrasos. Para além disto, o servidor era obrigado a criar várias ligações TCP para cada cliente, uma para enviar informação e várias para cada mensagem recebida \cite{WEBSOCKETRFC, websockethtml5}.
WebSocket Protocol foi desenhado para resolver este tipo de problemas. É um protocolo que fornece uma comunicação bidirecional, é baseado em \textit{sockets} e usa HTTP como camada de transporte. Desta forma é possível usar \textit{WebSockets} com soluções já existentes, visto que pode trabalhar sob portas HTTP como 80 e 443, o que facilita a integração deste protocolo com as soluções já existentes. Para além disso, este protocolo não está apenas limitado ao HTTP, o que permite fazer uma \textit{handshake} simples sob uma porta sem reinventar o protocolo \cite{WEBSOCKETRFC, websockethtml5}.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.6\textwidth]{images/websockets.png}
  \caption{Comparação de latência entre \textit{polling} e aplicações \textit{Websockets} \cite{websockethtml5v2}.}
  \label{fig:compar_websocket_poll}
\end{figure}

Na Figura \ref{fig:compar_websocket_poll} estão representados dois tipos de ligações: \textit{polling (HTTP)} e \textit{WebSockets}. Numa ligação \textit{polling} o \textit{browser} necessita de estar sempre a fazer pedidos, pois o servidor só responde no caso de receber esses pedidos. No caso do \textit{WebSocket} o \textit{browser} faz apenas um pedido e o servidor envia toda a informação em vários pacotes de dados, sem ter que receber qualquer pedido adicional por parte do \textit{browser}. Esta ligação entre o servidor e o \textit{web browser} é designada como uma ligação persistente, pois sempre que um deles precisar de enviar qualquer informação podem fazê-lo a qualquer momento.


\section{Sinalização}

Ao contrário do que acontece com uma infraestrutura completamente pensada para estabelecer uma ligação ou até mesmo controlar todo o \textit{media}, o mesmo não acontece quando se fala da sinalização. Para estabelecer uma chamada entre os utilizadores, é necessária uma negociação de parâmetros da sessão, descritos via SDP. 
A definição do método a utilizar fica a cargo da camada aplicacional, ou seja, a pessoa que está a desenvolver uma aplicação pode definir o método que quer para a sinalização. Apesar disso existem alguns protocolos que definem como deve ser feita a sinalização.

\subsection{\textit{SIP over WebSockets}}

O protocolo \textit{WebSockets}, descrito na secção \ref{sec:websocket}, permite a troca de mensagens entre clientes \textit{web} e o servidor, em tempo real e de uma forma persistente.
\textit{SIP over Websockets} é uma especificação que define um subprotocolo de \textit{Websocket}, permitindo a troca de mensagens \textit{SIP} entre um cliente e um servidor \textit{web}. Está definido na camada da aplicação e é um protocolo fiável. 

Existem duas entidades principais nesta especificação \cite{WebsocketDRAFT}:

\begin{itemize}

\item \textit{SIP WebSocket Client}: entidade capaz de abrir ligações de \textit{websockets} saída para o servidor e que comunica por \textit{WebSocket SIP subprotocol}.
\item \textit{SIP WebSocket Server}: entidade que escuta por ligações de entrada dos clientes e comunica por \textit{WebSocket SIP subprotocol}.

\end{itemize}

Em suma, este é um protocolo idêntico ao \textit{WebSocket}, mas que transporta mensagens \textit{SIP} entre as entidades já apresentadas.

\subsection{\textit{Jingle over WebSockets}}

\textit{Extensible Messaging and Presence Protocol (XMPP) Jingle} define um protocolo \textit{XMPP} para a gestão de sessões \textit{media peer-to-peer}, que permite fazer a gestão de múltiplos conteúdos em simultâneo \cite{jingleRFC}.
Este protocolo foi definido principalmente para comunicações em tempo real, como presença e chat, visto que, as mensagens trocadas por este protocolo não são enviadas em \textit{frames}, mas em \textit{streams XMPP}. A especificação \textit{Jingle} permite fazer a negociação de parâmetros de uma sessão entre dois clientes. Depois dessa negociação é criado um caminho de media onde são trocadas \textit{Jingle streams}, diretamente entre dois utilizadores.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.7\textwidth]{images/jingleprocess.png}
  \caption{Processo \textit{Jingle} \cite{jingleRFC}.}
  \label{fig:jingle}
\end{figure}

Em comparação ao que acontece com a sinalização com outros protocolos (e.g. \textit{SIP}), a sinalização é feita a partir de um caminho \textit{XMPP} e de \textit{Jingle media}.
Como este tipo de técnicas são baseadas em \textit{Hypertext Transfer Protocol (HTTP) long polling}, que envolve demasiado \textit{overhead}. Uma solução para este problema consiste em evitar \textit{XMPP} sobre \textit{HTTP}, o que indica que seria necessário usar \textit{XMPP} nativo. Porém os \textit{web browsers} ainda não suportam essas funcionalidades nativamente, para resolver esse problema existe o protocolo \textit{WebSocket}. À semelhança de \textit{"SIP sobre WebSockets"}, a solução \textit{Jingle} sobre \textit{WebSockets} permite enviar informação \textit{XMPP} entre os vários \textit{web browsers} e os servidores. Desta forma a comunicação e a troca de mensagens entre as várias identidades é feita de uma forma mais robusta e eficiente \cite{jingleoverwebsocketRFC}.

\subsection{\textit{JavaScript Session Establishment Protocol (JSEP)}}

O JSEP define de que forma as aplicações \textit{JavaScript} interagem com as funções \textit{Real Time Communication (RTC)}. Este protocolo é responsável pela forma  como os clientes podem recolher informação sobre o tipo de \textit{media} que suportam e que \textit{codecs} usam, o que fica definido num objeto \textit{SDP RTCSessionDescription}. O \textit{JSEP} fornece mecanismos de criação de \textit{offers} e \textit{answers}, como também de que forma podem ser aplicadas numa sessão \cite{jingleoverwebsocketRFC}. 
Este protocolo não define de que forma essa informação é trocada, ou seja, não é definido como essa informação é enviada ou recolhida do servidor \textit{web}, ficando isso a cargo do programador.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.7\textwidth]{images/JSEPmodel.png}
  \caption{Modelo \textit{JSEP} \cite{jingleoverwebsocketRFC}.}
  \label{fig:jingle}
\end{figure}

A figura \ref{fig:jingle} mostra o modelo utilizado pelo \textit{JSEP}, onde  as mensagens entre o \textit{browser} e a aplicação são objetos \textit{SDP}, e o protocolo que é usado para transmitir essa informação é especificado na camada aplicacional.

 \section{\textit{Network Address Translation (NAT)}}

Comparativamente com o protocolo \textit{Session Initiation Protocol (SIP)}, \textit{WebRTC} tem problemas \textit{NAT} e \textit{Firewall}, para isso são usados os mesmos protocolos (\textit{Interactivity Connectivity Establishment, Session Traversal Utilities for NAT e Traversal Using Relays around NAT}), para resolver alguns desses problemas.

\subsection{\textit{Interactive Connectivity Establishment (ICE)}}

\textit{ICE} é uma solução para \textit{streams} de \textit{media} baseadas em \textit{User Datagram Protocol (UDP)}, que sejam estabelecidas num modelo pedido/resposta. Este protocolo permite a vários clientes trocarem informação \textit{media} através de \textit{NAT}, sendo assim, estes conteúdos têm incluídos endereços \textit{IP} e portos, que por norma são problemáticos no que diz respeito sistemas com \textit{NAT}.

O \textit{ICE} é um protocolo que usa uma técnica conhecida como \textit{"hole punching"}, que foi definida para que os utilizadores possam trocar informação \textit{media} entre eles na presença de \textit{NAT}. Esta técnica nem sempre é fiável e poderá falhar e para este tipo de situações o protocolo \textit{ICE} tira proveito das funcionalidades do protocolo \textit{TURN}.

No que diz respeito à tecnologia \textit{WebRTC}, o protocolo permite a troca de \textit{media} entre dois clientes que estejam por trás de \textit{NAT}. Adicionalmente contém um método de verificação de comunicação, que permite prevenir ataques \textit{Denial of Service (DoS)}, ou seja, isto acontece porque não é enviada media enquanto a informação \textit{ICE} não seja trocada \cite{ICERFC}.

\subsection{\textit{Session Traversal Utilities for NAT (STUN)}}

\textit{Session Traversal Utilities} é um  protocolo normalizado que permite lidar com \textit{NAT}. Este protocolo fornece um mecanismo que permite a um cliente descobrir o endereço \textit{IP} e o porto alocado por \textit{NAT}. Este porto e o endereço \textit{IP} privado permitem que a ligação \textit{NAT} permaneça ativa.
As funcionalidades principais deste protocolo verificam conectividade entre dois agentes ou permitem a retransmissão de pacotes de dados entre dois agentes.
Este protocolo é usado na tecnologia \textit{WebRTC} para estabelecer uma sessão entre dois clientes, onde o \textit{web browser} funciona como cliente STUN e o servidor web como servidor STUN, por isso, são enviados à priori, pacotes de teste para estabelecer uma sessão, de forma a descobrir o caminho que se encontra por trás de \textit{NAT}, de endereços \textit{IP} e portos mapeados \cite{STUNRFC}.


\subsection{\textit{Traversal Using Relays around NAT (TURN)}}

É possível a partir de técnicas \textit{"hole punching"} descobrir um caminho direto de um cliente para o outro, atravessando \textit{NAT} como faz o protocolo \textit{ICE}. Este tipo de técnicas podem falhar caso os clientes que estejam por trás de \textit{NAT} não tenham um comportamento bem definido. Quando isto acontece, não é possível encontrar um caminho direto entre dois clientes, é necessário recorrer a serviços de um host intermediário que vai agir como um \textit{relay}. Normalmente este \textit{relay} situa-se numa rede pública (\textit{Internet}) e retransmite os pacotes entre os dois clientes, que se encontram por trás de \textit{NAT}.

Este tipo de especificação define o protocolo \textit{TURN}, em que o seu funcionamento passa por, um cliente \textit{TURN} fazer um pedido a outro cliente para que este aja como um servidor \textit{TURN}, de forma a que os pacotes que estão a ser transmitidos, possam ser reencaminhados para o seu destino. 
No caso do \textit{WebRTC} o \textit{web browser user agent} inclui um cliente \textit{TURN} e um servidor \textit{TURN}. É feito um pedido ao servidor \textit{TURN}, de um endereço \textit{IP} público e um porto que irá funcionar como endereço de um \textit{relay} de transporte \cite{TURNRFC}.

\section{\textit{Codecs}}

Ao nível aplicacional existem mecanismos adaptativos para lidar com a qualidade de serviço e a degradação da mesma, exemplo disso são os \textit{codecs} de vídeo e áudio. 

No desenvolvimento destas aplicações, a escolha dos protocolos de transporte e dos mecanismos de compressão de voz e vídeo tem de ser rigorosa, por forma a obter o melhor desempenho fim-a-fim. Em termos de \textit{hardware} são usados codificadores e descodificadores, que por sua vez poderão executar \textit{software} baseado em algoritmos de compressão e descompressão.

Para garantir a interoperabilidade entre os vários clientes \textit{WebRTC}, foi necessário definir requisitos, no que diz respeito a \textit{codecs} áudio e vídeo. Os clientes \textit{WebRTC} que são desenvolvidos devem garantir como requisitos de áudio \textit{Pulse Code Modulation (PCMA/PCMU)}, \textit{telefone event} e \textit{Opus} e \textit{G.711} como \textit{codecs} de áudio. No que toca aos \textit{codecs} de vídeo devem ser garantidos 10 \textit{frames} por segundo e uma resolução mínima de 320x240, suportando também resoluções 1280x720, 720x480, 1024x768, 800x600,640x480 e 640x360 \cite{webrtccodec}. \textit{RTCWeb} definiu dois \textit{codecs} áudio obrigatórios: \textit{Opus} e \textit{G.711}.
No entanto, ainda não foram definidos quais os \textit{codecs} de vídeo obrigatórios, dependendo assim, da implementação do \textit{web browser}. A tabela \ref{table:codecandbrowsers} mostra quais os \textit{codecs} que cada um dos \textit{web browsers} usa, tanto áudio como vídeo.

\begin{table} [h!]
	\caption{Tabela de suporte \textit{codecs} nos diferentes \textit{web browsers} \cite{webrtc2sip}.}
	\label{table:codecandbrowsers}
	\begin{center}
	    \begin{tabular}{ | l | l | l | l | l | p{5cm} |}
	    \hline
	     & Chrome & Firefox & Internet Explorer & Opera \\ \hline
	    Vídeo & VP8 & VP8 & H.264 AVC & VP8 \\ \hline
	    Áudio & Opus, G.711 & Opus, G.711 & Opus, G.711 & Opus, G.711 \\ \hline
	    \end{tabular}
	\end{center}
\end{table}

%\begin{center}
%	    \begin{tabular}{ | l | l | l | l | l | p{5cm} |}
%	    \hline
%	    API & Internet Explorer & Firefox & Chrome & Opera \\ \hline
%	    MediaStream & Não Suporta & Suporta \textgreater v17 & Suporta \textgreater v18 & Suporta \textgreater v12 \\ \hline
%	    Peer Connection & Não Suporta & Suporta \textgreater v22 & Suporta \textgreater v20 & Sem Informação \\ \hline
%	    RTC Data Channels & Não Suporta & Suporta \textgreater v22 & Suporta \textgreater v26 & Sem Informação \\ 
%	    \hline
%	    \end{tabular}
%	\end{center}


\section{Sumário}

Neste capítulo descreveram-se, de um modo geral, a arquitetura \textit{WebRTC}, as \textit{APIs} que constituem essa tecnologia (\textit{PeerConnection}, \textit{Media Streams}, \textit{RTC Data Channels}) e de que forma podem ser usadas. Abordou-se a compatibilidade desta tecnologia e também da tecnologia \textit{HTML5} nos \textit{web browsers}, visto que por norma são usadas, atualmente, em conjunto para desenvolver aplicações \textit{web}. 

Foram discutidos alguns métodos que podem ser usados para a sinalização e de que forma podem ser transportados de uma aplicação para outra, para haver negociação de parâmetros.
Por fim descreve-se o protocolo \textit{WebSockets} e de que forma se diferencia dos tradicionais protocolos que os servidores \textit{web} usam.

Discutiu-se ainda o problema de \textit{NAT} em WebRTC e quais os protocolos que ajudam a resolver esse problema, como \textit{ICE}, \textit{TURN} e \textit{STUN}. A questão relacionada com os \textit{codecs} de áudio e vídeo também foi abordada, visto que os diferentes \textit{browsers} podem usar diferentes \textit{codecs}, e deve haver uma interação e negociação para que possa haver interoperabilidade. Discutiram-se ainda alguns protocolos usados para a troca de media e questões de segurança, protocolos como \textit{SDP}, \textit{SRTP} e \textit{DTLS}.